<?php declare(strict_types = 1);

// TODO REVIZE

namespace Luky\Toolkit\Collection;

use Luky\Toolkit\Collection\Exception\InvalidMemberTypeException;

/**
 * @template T of object
 *
 * @property T[] $data
 *
 * @method T current()
 * @method T next()
 */
abstract class MutableCollection extends ObjectIterator
{
    /**
     * @param array<int, T|null> $data
     */
    final public function __construct(array $data = [])
    {
        $data = \array_filter($data, fn($item): bool => $item !== null);

        foreach ($data as &$candidate) {
            $this->assertType($candidate);
        }

        $this->data = $data;
    }


    /**
     * @return class-string<T>
     */
    abstract public function getType(): string;


    /**
     * @param T $item
     */
    public function add(object $item): self
    {
        $this->assertType($item);

        $this->data[] = $item;

        return $this;
    }


    /**
     * @param T $candidate
     */
    protected function assertType($candidate): void
    {
        $type = $this->getType();

        if ($candidate instanceof $type === false) {
            throw new InvalidMemberTypeException(
                \sprintf(
                    'Provided item has class "%s", allows only "%s"',
                    $candidate::class,
                    $type,
                ),
            );
        }
    }


    /**
     * @return T[]
     */
    public function toArray(): array
    {
        return \iterator_to_array($this);
    }
}
