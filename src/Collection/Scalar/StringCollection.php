<?php declare(strict_types = 1);

namespace Luky\Toolkit\Collection;

/**
 * @property string[] $data
 *
 * @method string current()
 * @method string next()
 */
final class StringCollection extends ObjectIterator
{
    /**
     * @param string[] $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as &$candidate) {
            $this->add($candidate);
        }
    }


    public function add(string $item): self
    {
        $this->data[] = $item;

        return $this;
    }
}
