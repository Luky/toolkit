<?php declare(strict_types = 1);

namespace Luky\Toolkit\Collection;

/**
 * @property int[] $data
 *
 * @method int current()
 * @method int next()
 */
final class IntCollection extends ObjectIterator
{
    /**
     * @param int[] $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as &$candidate) {
            $this->add($candidate);
        }
    }


    public function add(int $item): self
    {
        $this->data[] = $item;

        return $this;
    }
}
