<?php declare(strict_types = 1);

// TODO REVIZE

namespace Luky\Toolkit\Collection;

abstract class ObjectIterator implements \ArrayAccess, \Iterator, \Countable
{
    /**
     * @var array<int|string, object|int|string|array<int|string, object|int|string|null>|null>
     */
    protected array $data = [];


    /**
     * @param array<int|string, object|int|string|array<int|string, object|int|string|null>|null> $data
     */
    abstract function __construct(array $data = []);


    public static function fromArgs(mixed ...$args): static
    {
        return new static($args);
    }


    public function isEmpty(): bool
    {
        return $this->count() === 0;
    }


    public function count()
    {
        return \count($this->data);
    }


    public function current()
    {
        return \current($this->data);
    }


    public function next(): int|string|bool|object|null
    {
        return \next($this->data);
    }


    public function key()
    {
        return \key($this->data);
    }


    public function valid(): bool
    {
        return \key($this->data) !== null;
    }


    public function rewind(): void
    {
        \reset($this->data);
    }


    public function offsetExists($offset): bool
    {
        return isset($this->data[$offset]);
    }


    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }


    public function offsetSet($offset, $value): int|string|bool|object|null
    {
        return $this->data[$offset] = $value;
    }


    public function offsetUnset($offset): void
    {
        unset($this->data[$offset]);
    }


    public function getFirst(): int|string|bool|object|null
    {
        $this->rewind();

        return $this->current();
    }
}
