<?php declare(strict_types = 1);

namespace Luky\Toolkit\FileSystem;

use Nette\Utils\FileSystem;

class File
{
    protected string $fileName;

    private string $charset = 'utf-8';

    private ResouceContainer|null $resource = null;


    public function __construct(protected string $filePath, protected string $mineType, string|null $fileName = null)
    {
        $this->fileName = $fileName ?? (new \SplFileInfo($filePath))->getFilename();
    }


    public static function fromPath(string $path): self
    {
        $fileInfo = new \SplFileInfo($path);

        return new self(
            $path,
            \mime_content_type($path),
            $fileInfo->getFilename(),
        );
    }


    public function __destruct()
    {
        if ($this->resource !== null) {
            $this->unlock();
        }
    }


    public function lock(ResouceContainer $resource, bool $exclusive = false): void
    {
        $this->resource = $resource;
        \flock($resource->get(), $exclusive ? \LOCK_EX : \LOCK_SH);
    }


    public function unlock(): void
    {
        if ($this->resource === null) {
            return;
        }

        \flock($this->resource->get(), \LOCK_UN);
    }


    public function prepare(): void
    {
        FileSystem::createDir(\dirname($this->getFilePath()));
    }


    public function getFilePath(): string
    {
        return $this->filePath;
    }


    public function getMineType(): string
    {
        return $this->mineType;
    }


    public function getFileName(): string
    {
        return $this->fileName;
    }


    public function isExists(): bool
    {
        return \file_exists($this->filePath);
    }


    public function getContent(): string
    {
        return FileSystem::read($this->filePath);
    }


    public function getCharset(): string
    {
        return $this->charset;
    }


    public function getContentSize(): int
    {
        return \filesize($this->filePath) * 8;
    }
}
