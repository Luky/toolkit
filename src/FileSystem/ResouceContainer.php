<?php declare(strict_types = 1);

// TODO REVIZE

namespace Luky\Toolkit\FileSystem;

final class ResouceContainer
{
    public function __construct(private mixed $pointer)
    {
        if (\is_resource($pointer) === false) {
            throw new \InvalidArgumentException(
                \sprintf('Expected resource got %s', $pointer::class),
            );
        }
    }


    /**
     * @return resource
     */
    public function get()
    {
        return $this->pointer;
    }
}
