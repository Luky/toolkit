<?php declare(strict_types = 1);

namespace Luky\Toolkit\FileSystem;

use Nette\StaticClass;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

final class XlsFile
{
    use StaticClass;

    /**
     * @param array<int, array> $data
     */
    public static function fromArray(File $outputFile, array $data): void
    {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $writer = new Xlsx($spreadsheet);
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->fromArray($data, null, 'A1');

        $outputFile->prepare();

        $writer->save($outputFile->getFilePath());
    }


    public static function fromCsv(File $outputFile, File $csv): void
    {
        $data = [];
        $fp = \fopen($csv->getFilePath(), 'rb');

        while ($row = \fgetcsv($fp)) {
            $data[] = $row;
        }

        \fclose($fp);

        self::fromArray($outputFile, $data);
    }
}
