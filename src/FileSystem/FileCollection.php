<?php declare(strict_types = 1);

namespace Luky\Toolkit\FileSystem;

use Luky\Toolkit\Collection\MutableCollection;

/**
 * @extends MutableCollection<File>
 *
 * @method File current()
 * @method File next()
 * @method File getFirst()
 * @method add(File $item)
 */
final class FileCollection extends MutableCollection
{
    public function getType(): string
    {
        return File::class;
    }


    /**
     * @return array<int, array{path: string, name:string, type:string}>
     */
    public function asArray(): array
    {
        $data = [];

        foreach ($this as $file) {
            $data = [
                'path' => $file->getFilePath(),
                'name' => $file->getFileName(),
                'type' => $file->getMineType(),
            ];
        }

        return $data;
    }


    public static function fromArray(array $data): self
    {
        $self = new self();

        foreach ($data as [$path, $name, $type,]) {
            $self->add(new File($path, $type, $name));
        }

        return $self;
    }
}
